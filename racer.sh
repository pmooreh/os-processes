

name="$1"
runs=0

run_test()
{
        runs=$((runs + 1))
        echo -n "$runs: "
        make -s "$name"
}

if [ -z "$name" -o ! -e "$name.ok" ]; then
        echo "Usage: $0 testX"
        echo 'Run in the root directory of project and ensure testX.ok exists'
        exit 1
fi

make -s clean
run_test
while [ ! -s "$name.diff" ]; do
        run_test
done

exit 0