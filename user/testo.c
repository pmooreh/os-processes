#include "libc.h"

int main() {
    putstr("*** hello\n");


    putstr("*** open hello.txt twice\n");
	
	int fd0 = open("hello.txt");
	int fd1 = open("hello.txt");
    
	SHOW(fd0);
	SHOW(fd1);

	putstr("*** close first fd\n");

	close(fd0);

	putstr("*** reopen hello.txt\n");

	int fd2 = open("hello.txt");

	SHOW(fd2);

    close(fd1);
	close(fd2);

    return 0;
}

