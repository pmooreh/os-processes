#include "libc.h"

int main(int argc, char* argv[]) {
    int a;
    int b;

    pipe(&a,&b);

    int id = fork();
    if (id == 0) {
        /* child */
        while (1) {
            char c;
            int n = read(b,&c,1);
            if (n != 1) {
                putstr("*** bad rc:"); putdec(n); putstr("\n");
                break;
            }
            if (c == 'e') {
                c = '\0';
                write(b, &c, 1);
                break;
            }

            putstr("*** child: "); putch(c); putstr("\n");
            c++;
            write(b,&c,1);
        }
    } else {
        /* parent */
        char c = 'a';
        write(a,&c,1);
        while (1) {
            int n = read(a,&c,1);
            if (n != 1) {
                putstr("*** bad rc:"); putdec(n); putstr("\n");
                break;
            }
            if (c == '\0') break;

            putstr("*** parent: "); putch(c); putstr("\n");
            c++;
            write(a, &c, 1);
        }
    }

    return 0;
}

