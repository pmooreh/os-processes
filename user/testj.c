#include "libc.h"


void one(int out, char c) {
//    putstr("*** parent: "); putch(c); putstr("\n");
    write(out,&c,1);
}


int main(int argc, char* argv[]) {
    int out;
    int in;

    pipe(&out,&in);    

    int id = fork();
    if (id == 0) {
        /* child */
        close(out);
        while (1) {
            char c;
            int n = read(in,&c,1);
            if (n == 0) break;
            
            putstr("*** child: "); putch(c); putstr("\n");
        }        
    } else {
        /* parent */
        close(in);
   
        one(out,'h');
        one(out,'e');
        one(out,'l');
        one(out,'l');
        one(out,'o');
    }

    return 0;
}
