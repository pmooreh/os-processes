#include "libc.h"

int main() {
    putstr("*** hello\n");

    int sem = semaphore(1);

    putstr("*** fork\n");
    fork();

    down(sem);
    putstr("*** there\n");
    up(sem);

    return 0;
}
