#include "libc.h"

static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

void putch(char c) {
    write(1,&c,1);
}

char getch(void) {
    char c;
    read(0,&c,1);
    return c;
}

void putstr(char* p) {
    char c;
    while ((c = *p++) != 0) putch(c); 
}

char* getstr() {
    long sz = 0;
    char *p = 0;
    long i = 0;

    while (1) {
        if (i >= sz) {
            sz += 10;
            p = realloc(p,sz+1);
            if (p == 0) return 0;
        }
        char c = getch();
        putch(c);
        if (c == 13) {
            putstr("\n");
            p[i] = 0;
            return p;
        }
        p[i++] = c;
    }
}

void puthex(long v) {
    for (int i=0; i<sizeof(long)*2; i++) {
          char c = hexDigits[(v & 0xf0000000) >> 28];
          putch(c);
          v = v << 4;
    }
}

void putdec(int v) {
    if (v < 0) {
        putch('-');
        v = -v;
        if (v < 0) {
            putstr("2147483647");
            return;
        }
    }
       
    if (v >= 10) {
        putdec(v / 10);
    }
    putch(hexDigits[v % 10]);
}

void saystr(char* s) {
    putstr("*** ");
    putstr(s);
    putstr("\n");
}

void saych(char* s, char c) {
    putstr("*** ");
    putstr(s);
    putstr(" ");
    putch(c);
    putstr("\n");
}

void saydec(char* s, int v) {
    putstr("*** ");
    putstr(s);
    putstr(" ");
    putdec(v);
    putstr("\n");
}

void sayhex(char* s, int v) {
    putstr("*** ");
    putstr(s);
    putstr(" ");
    puthex(v);
    putstr("\n");
}


ssize_t readAll(int fd, void* buf, size_t nbyte) {
    size_t togo = nbyte;

    while (togo > 0) {
        ssize_t n = read(fd,buf,togo);
        if (n < 0) return n;
        if (n == 0) break;
        togo -= n;
    }
    return nbyte - togo;
}

extern int doExecv(char* prog, int n, char** args);

int execv(char* prog, char** args) {
    int length = 0;
    while (args[length] != 0) {
        length++;
    }

    return doExecv(prog, length, args);
}

extern void threadLeave();

int threadCreate(int (*func)(void*), void* arg) {
	// make a stack with length size
	int size = 1024;
	int* stack = malloc(4 * size);

	// set the stack up so that the arg is in the correct place
	stack[size - 1] = (int)arg; // the arg goes at 4(%esp)
	stack[size - 2] = ((int)(&threadLeave)); // return to threadLeave
	int eip = (int)(&stack[size - 2]);

	return thread((int)func, eip);
}
