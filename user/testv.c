#include "libc.h"

int numThreads = 26;
int strLen = 10;
char* strArr[10];
int sema = -1;

int func(void* arg) {
	int x = (int) arg;
    //SHOW(x);
	char* str  = malloc(strLen);
	//putstr("hello 1\n");

	for (int j = 0; j < strLen; j++) {
		//SHOW(j);
		str[j] = 'a' + x;
	}

	//putstr("hello 2\n");

	str[strLen - 1] = 0;

	//putstr("hello 3\n");

//	down(sema);
	putstr(str);
	putstr("\n");
	up(sema);

	//putstr("hello 4\n");

	return 0;    
}

int main(int argc, char* argv[]) {
    putstr("*** hello\n");

	sema = semaphore(1);

	for (int i = 0; i < numThreads; i++) {
		threadCreate(func, (void*)i);
	}

    return 0;
}

