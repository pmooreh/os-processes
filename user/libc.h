#ifndef _LIBC_H_
#define _LIBC_H_

#include "sys.h"

#define MISSING() do { \
    putstr("\n*** missing code at"); \
    putstr(__FILE__); \
    putdec(__LINE__); \
} while (0)

extern void* malloc(size_t size);
extern void free(void*);
extern void* realloc(void* ptr, size_t newSize);

void* memset(void* p, int val, size_t sz);
void* memcpy(void* dest, void* src, size_t n);

extern void putch(char c);
extern void putstr(char *p);
extern void putdec(int v);
extern void puthex(long v);

extern char getch(void);
extern char* getstr(void);

extern void saych(char* s, char v);
extern void saydec(char* s, int v);
extern void sayhex(char* s, int v);
extern void saystr(char* s);

#define SHOW(x) saydec(#x,(x))

extern ssize_t readAll(int fd, void* buf, size_t nbyte);

extern int execv(char* prog, char** args);

extern int threadCreate(int (*func)(void*arg), void* arg);

#endif
