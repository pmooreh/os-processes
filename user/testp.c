#include "libc.h"

int main() {
    putstr("*** hello\n");

	putstr("*** using invalid file descriptors\n");

	int badFd = 50;
	char buf[10];

	close(badFd);
	close(badFd);
	read(badFd, buf, 10);
	write(badFd, buf, 10);
	seek(badFd, 1000);
	len(badFd);

	putstr("*** invalid semaphore identifier\n");

	int badSema = 50;

	up(badSema);
	down(badSema);

    return 0;
}

