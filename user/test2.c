#include "libc.h"

int main() {
    putstr("*** hello\n");

    putstr("*** create semaphore\n");
    int sem = semaphore(1);
    putstr("*** say down\n");
    down(sem);
    putstr("*** say down, again\n");

    return 0;
}
