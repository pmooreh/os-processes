#include "libc.h"

int main(int argc, char* argv[]) {

	int id = fork();
	
	if(id == 0) {
		join(0);
		putstr("*** hello again\n");
	}
   
	else {
		putstr("*** hello\n");
	}
    
	return 0;
}

