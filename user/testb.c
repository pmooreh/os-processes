#include "libc.h"

int main() {
    saystr("hello");

    saystr("open bad file");
    int fd = open("notthere");
    SHOW(fd);

    saystr("len of bad file");
    int n = len(fd);
    SHOW(n);

    char buf[100];

    saystr("read from bad file");
    int c = read(fd,buf,3);
    SHOW(c);    
    
    saystr("survive closing nothing");
    close(fd);

    saystr("open good file");
    fd = open("hello.txt");
    SHOW(fd);

    saystr("open again");
    fd = open("hello.txt");
    SHOW(fd);

    close(2);
    close(3); 

    saystr("len of closed file");
    n = len(2);
    SHOW(n);

    saystr("read from closed file");
    n = read(3,buf,3);
    SHOW(n);

    saystr("open it again");
    fd = open("hello.txt");
    SHOW(fd);
    
    c = readAll(fd,buf,1000000);
    SHOW(c);
    buf[c] = 0;
    saystr(buf);

    return 0;
}
