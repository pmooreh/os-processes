#include "libc.h"

int main(int argc, char* argv[]) {
	int n = 6;
	int sema = semaphore(1);

    int id = 0;
	for (int i = 0; i < n && id == 0; i++) {
		id = fork();
	}
	
	if(id != 0){
		for (int i = 0; i < n + 2 - id; i++) {
			join(n + 1 - i);
		}
	}
    
	down(sema);
    putstr("*** child thread id: "); putdec(id); putstr("\n");
    up(sema);

    return 0;
}
