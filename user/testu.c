#include "libc.h"

int main(int argc, char* argv[]) {
	int batchSize = 16;
	int numRounds = 125;
	int sema = semaphore(1);

    int id = 0;
	for (int round = 0; round < numRounds; round++) {
		for (int i = 0; i < batchSize && id == 0; i++) {
			down(sema);
			
			id = fork();
			int s = semaphore(0);
			s = s + 0;

			up(sema);
		}
		
		if(id > 1) {
			if (id % 100 == 0) {
				down(sema);
				SHOW(id);
				up(sema);
			}
			exit(0);
		}
	}

	down(sema);
    putstr("*** last thread id: "); putdec(id); putstr("\n");
    up(sema);

    return 0;
}

