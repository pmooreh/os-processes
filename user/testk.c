#include "libc.h"

int y = 100;

int func(void* arg) {
    int x = (int) arg;
    saydec("child x",x);
    saydec("child y",y);
    y = 1000;
    return x+1;    
}

int main(int argc, char* argv[]) {
    putstr("*** hello\n");
    saydec("parent y",y);

    int id = threadCreate(func,(void*)100);
    int rc = join(id);
    saydec("parent rc",rc);
    saydec("parent y",y);
    return 0;
}
