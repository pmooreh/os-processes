#include "libc.h"

int doRead(void *arg)
{
        int s = (int)arg;
        int nbyte;
        char buf[100];
        char *ptr = buf;
        down(s);
        do {
                nbyte = read(2, ptr, 9999);
                ptr += nbyte;
        } while (nbyte > 0);
        putstr(buf);
        return (int)ptr - (int)buf;
}
int main(int argc, char **argv)
{
        int s = semaphore(0);
        int tid = threadCreate(doRead, (void*)s);
        int ret;
        open("f1.txt");
        up(s);
        ret = join(tid);
        putstr("*** read ");
        putdec(ret);
        putstr(" bytes\n");
        return 0;
}