#include "libc.h"

int main(int argc, char* argv[]) {
	int sema = semaphore(0xffffffff);
    
	up(sema);
	down(sema);
    putstr("*** hello\n");

    return 0;
}

