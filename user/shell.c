#include "libc.h"

char** split(char *str) {
    long nWords = 0;
    long nArgs = 0;
    char **args = 0;
    int i = 0;

    int space = 1;

    while (1) {
        char c = str[i];
        if (c == 0) break;

        if (c == ' ') {
            str[i] = 0;
            space = 1;
        } else {
            if (space) {
                if (nArgs <= nWords) {
                    nArgs = nWords + 10;
                    args = (char**) realloc(args, (nArgs + 1) * 4);
                    if (args == 0) return 0;
                }
                args[nWords] = &str[i];
                nWords ++;
            }
            space = 0;
        }
        i++;
    }

    if (args) {
        args[nWords] = 0;
    }

    return args;
        
}

void notFound(char* cmd) {
    putstr("*** ");
    putstr(cmd);
    putstr(": command not found\n");
}

int main() {
    while (1) {
        putstr("*** shell> ");
        char* in = getstr();
        char **args = 0;

        if (in == 0) goto done;

        args = split(in);
        if (args == 0) goto done;
        
        char *cmd = args[0];
        if (cmd == 0) goto done;

        int fd = open(cmd);
        if (fd < 0) {
            notFound(cmd);
            goto done;
        }

        char* magic = malloc(4);
        readAll(fd, magic, 4);
        
        int id = fork();
        if (id == 0) {
		   	// CHILD. execute stuff!

			// if not an elf file, cat the file used as an invalid command
			if (!(magic[0] == 0x7f && magic[1] == 'E' && magic[2] == 'L' && magic[3] == 'F')) {
				exec("cat", 2, "cat", args[0]);
			}

			// otherwise, this is an elf file. execute it with the commands supplied!
			else {
				execv(cmd, args);
			}
        }
        else {
			// PARENT. join with the child, and output bad rc (exit status) if encountered
            int rc = join(id);

			// if the exit status is bad, output it
			if (rc != 0) {
				putstr("*** rc:");
				putdec(rc);
				putstr("\n");
			}
        }

done:
        if (in) free(in);
        if (args) free(args);
    }
    return 0;
}
