#ifndef _LOCKS_H_
#define _LOCKS_H_

#include "stdint.h"
#include "pic.h"
#include "queue.h"
#include "thread.h"
#include "refs.h"

class Thread;

inline uint32_t cmpxchg(volatile uint32_t *p,
    uint32_t cmpVal, uint32_t setVal) {
  uint32_t res;
  __asm__ volatile( "lock cmpxchgl %2,%1"
    : "=a" (res), "+m" (*p)
    : "r" (setVal), "0" (cmpVal)
    : "memory", "cc");
  return res;
}
    
class SpinLock {
    volatile uint32_t flag;
public:
    SpinLock();
    bool tryLock();
    bool lock();
    void unlock(bool was);
};

class Semaphore {
    uint32_t count;
    uint32_t overflow;
	SpinLock lock;
    StrongQueue<Thread> waiting;
public:
    Semaphore(uint32_t count);
    void down();
    void up();
};

class BlockingLock {
    Semaphore sem;
public:
    BlockingLock();
    void lock();
    void unlock();
};

class Event {
    Semaphore sem;
public:
    Event();
    virtual ~Event();

    void wait();
    void signal();
};

class Barrier {
    Semaphore sem;
    uint32_t togo;
    BlockingLock lock;
public:
    Barrier(uint32_t n);

    void sync();
};

template <typename T>
class BoundedBuffer {
    T *data;
    int n;
    Semaphore nEmpty;
    Semaphore nFull;
    BlockingLock lock;
    int head;
    int tail;

public:

    BoundedBuffer(int n) : data(new T[n]), n(n), nEmpty(n), nFull(0), lock(), head(0), tail(0) {}
    ~BoundedBuffer() {
        if (data != nullptr) {
            delete []data;
            data = nullptr;
        }
   }

    void put(T v) {
        nEmpty.down();
        lock.lock();
    
        data[tail % n] = v;
        tail ++;

        lock.unlock();
        nFull.up();
    } 

    T get(void) {
        nFull.down();
        lock.lock();
        
        T v = data[head % n];
        head ++;

        lock.unlock();
        nEmpty.up();
        return v;
    }

};

#endif
