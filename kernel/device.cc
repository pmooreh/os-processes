#include "device.h"

size_t BlockIO::read(size_t offset, void* buf, size_t nbyte) {
    if (nbyte == 0) return 0;
    uint32_t blockNum = offset / BLOCK_SIZE;
    uint32_t offsetInBlock = offset % BLOCK_SIZE;
    uint32_t left = BLOCK_SIZE - offsetInBlock;
    if (nbyte < left) {
        left = nbyte;
    }

    StrongPtr<BlockBuffer> buffer = readBlock(blockNum);

    buffer->ready->wait();

    memcpy(buf,&buffer->data[offsetInBlock],left);

    return left;
}

size_t BlockIO::readAll(size_t offset, void* buf, size_t nbyte) {
    size_t togo = nbyte;
    char* ptr = (char*)buf;

    while (togo > 0) {
        size_t n = read(offset,ptr,togo);
        if (n == 0) break;
        if (n > togo) Debug::panic("%d > %d",n,togo);
        togo -= n;
        ptr += n;
        offset += n;
    }
    return nbyte - togo;
}



BlockBuffer::BlockBuffer(uint32_t id, uint32_t blockNum) :
    ready(new Event()),
    data(new char[BlockDevice::BLOCK_SIZE]),
    id(id),
    blockNum(blockNum)
{
}


BlockBuffer::~BlockBuffer() {
    if (data != nullptr) {
        delete []data;
    }
}
