#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "refs.h"
#include "vmm.h"
#include "locks.h"

class AddressSpace;
class File;
class BlockingLock;
class Semaphore;
class Event;

template <class T>
class Collection;

class OpenFile {
public:
    size_t offset;
    StrongPtr<File> file;
    StrongPtr<BlockingLock> lock;

    OpenFile(StrongPtr<File> file);
    ssize_t read(void* buf, size_t nbyte);
    ssize_t write(void* buf, size_t nbytes);
    size_t seek(size_t newOffset);
};

class Process {
    
public:

    Process();
    virtual ~Process();

    static constexpr int NFILES = 100;
	static constexpr int NSEMAPHORES = 100;

    /* return the kernel process */
    static StrongPtr<Process> kernel();

    /* initialize the process subsystem */
    static void init();

    StrongPtr<AddressSpace> addressSpace;
    const int id;

	// array of open files, array of semaphores
    StrongPtr<OpenFile> files[NFILES];
	StrongPtr<Semaphore> semaphores[NSEMAPHORES];

	// lock for the files and semaphores
	StrongPtr<BlockingLock> fileLock;
	StrongPtr<BlockingLock> semaLock;

	// file methods
    int addFile(StrongPtr<File> file);
    uint32_t getFileLength(int fileDescriptor);
    ssize_t readFile(int fileDescriptor, void* buf, size_t bytes);
    void closeFile(int fileDescriptor);
    size_t seekFile(int fileDescriptor, size_t offset);

	// semaphore methods
	int addSemaphore(int count);
	int semaUp(int semId);
	int semaDown(int semId);
	StrongPtr<Semaphore> getSemaphore(int id);	

	// forking methods
    void setSharedInfo(StrongPtr<Process> parent);
};

#endif
