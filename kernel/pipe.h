#ifndef _PIPE_H_
#define _PIPE_H_

/* pipe file */

#include "io.h"
#include "locks.h"
#include "fs.h"
#include "debug.h"

class Pipe : public File {

	StrongPtr<BoundedBuffer<char>> writeBuffer;
	StrongPtr<BoundedBuffer<char>> readBuffer;


public:

	Pipe(StrongPtr<BoundedBuffer<char>> read, StrongPtr<BoundedBuffer<char>> write)
		: writeBuffer(write), readBuffer (read) {}

    
	/* file methods */
	virtual uint32_t getLength() override {
		return (uint32_t) 0xffffffff;
    }

    virtual uint32_t getType() override {
        return 1; // is this correct? doesn't really matter!
    }

    virtual size_t read(size_t offset, void* buf, size_t nbyte) override {
    	
    	size_t bytesRead = 0;

    	while (bytesRead < nbyte) {
			((char*)buf)[bytesRead] = readBuffer->get();
			bytesRead++;
		}

		return bytesRead;
    }

    virtual size_t write(size_t offset, void* buf, size_t nbyte) override {

    	size_t bytesWritten = 0;

		while (bytesWritten < nbyte) {
			writeBuffer->put(((char*)buf)[bytesWritten]);
			bytesWritten++;
		}

		return bytesWritten;
	}
};

#endif
