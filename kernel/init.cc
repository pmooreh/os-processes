#include "debug.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "heap.h"
#include "random.h"
#include "config.h"
#include "u8250.h"
#include "ide.h"
#include "vmm.h"
#include "sys.h"
#include "init.h"

Config config;

extern "C" void kernelInit(void) {
    U8250 uart;
    Debug::init(&uart);
    Debug::debugAll = false;
    Debug::printf("\nHello, is there anybody out there?\n");

    /* discover configuration */
    configInit(&config);

    /* Initialize heap */
    heapInit((void*)0x100000,0x200000);

    /* Initialize physical memory */
    PhysMem::init(0x300000,0x400000);

    /* initialize random number generator */
    randomInit(0);

    /* initialize processes */
    Process::init();

SYS::init();

    /* initialize threads */
    Thread::init();

    /* initialize idt */
    IDT::init();

    /* initialize pic */
    picInit();

    /* initialize pit */
    Pit::init(10000);

    /* initialize IDE */
    IDE::init();

//    /* initialize system calls */
//    SYS::init();

    /* enable interrupts */
    picEnable();

    /* stdin, stdout */
	StrongPtr<File> uartForUser = StrongPtr<File>(new U8250());
	StrongPtr<Process> p = Thread::current()->process;
	p->addFile(uartForUser);
	p->addFile(uartForUser);
	p.reset();
	uartForUser.reset();
    
	kernelMain();

    Thread::exit();

    Debug::panic("should never see this");
}
