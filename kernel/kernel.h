#ifndef _KERNEL_H_
#define _KERNEL_H_

#include "refs.h"

class Directory;

struct KernelInfo {
    StrongPtr<Directory> rootDir;

    KernelInfo() {}
};

extern KernelInfo *kernelInfo;

#endif
