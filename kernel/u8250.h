#ifndef _U8250_H_
#define _U8250_H_

/* 8250 */

#include "io.h"
#include "locks.h"
#include "fs.h"

class U8250 : public File, public OutputStream<char> {
    BlockingLock getLock;
    BlockingLock putLock;
public:
    U8250() : getLock(), putLock() {
    }
    virtual void put(char ch);
    virtual char get();

    /* File methods */
    virtual uint32_t getLength() override {
        return (uint32_t) 0xffffffff;
    }

    virtual uint32_t getType() override {
        return 1;
    }

    virtual size_t read(size_t offset, void* buf, size_t nbyte) override {
        if (nbyte == 0) return 0;
        char* p = (char*) buf;
        *p = get();
        return 1;
    }

    virtual size_t write(size_t offset, void* buf, size_t nbyte) override {
        if (nbyte == 0) return 0;
        char* p = (char*) buf;
        put(*p);
        return 1;
    }


};

#endif
